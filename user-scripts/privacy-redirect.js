/* Targets taken from the privacy-redirect webextension
	 This isn't great, as it does still load the webpage before
	 redirecting, however it is better than nothing. */

const youtube_targets = [
		"m.youtube.com",
		"youtube.com",
		"img.youtube.com",
		"www.youtube.com",
		"youtube-nocookie.com",
		"www.youtube-nocookie.com",
		"youtu.be",
		"s.ytimg.com",
		"music.youtube.com",
];

const twitter_targets = [
		"twitter.com",
		"www.twitter.com",
		"mobile.twitter.com",
		"pbs.twimg.com",
		"video.twimg.com",
];

const reddit_targets = [
		"reddit.com",
		"www.reddit.com",
		"np.reddit.com",
		"new.reddit.com",
		"amp.reddit.com",
		"i.redd.it",
];

const instagram_targets = [
		"instagram.com",
		"www.instagram.com",
		"help.instagram.com",
		"about.instagram.com",
];

const targets = [
		[youtube_targets, "https://invidious.fdn.fr"],
		[twitter_targets, "https://nitter.fdn.fr"],
		[reddit_targets, "https://teddit.net"],
		[instagram_targets, "https://insta.trom.tf"],
];

for (i = 0; i < targets.length; i++) {
		if (targets[i][0].indexOf(window.location.hostname) > -1) {
				document.write('<script type="text/undefined">')
				location.replace(targets[i][1] + window.location.pathname);
		}
}
