chorizo-usage(1)

# NAME
chorizo-usage - extended usage hints

# DESCRIPTION
*chorizo* is a simple web browser using GTK+ 3, GLib and WebKit2GTK+. This
manpage contains additional hints and pointers regarding its usage.

# KEYBINDINGS AND CONFIGURATION
For this information, please refer to *chorizo-config*(5).

# DOWNLOAD MANAGER
Open the download manager using the appropriate hotkey. A new window listing
your downloads will appear. Clicking on an item will remove it from the list and
**if needed** cancel the download.

There's no file manager integration, nor does *chorizo* delete, overwrite or
resume downloads. If a file already exists, it won't be touched. Instead, the
new file name will have a suffix such as *.1*, *.2*, *.3*, and so on.

# USER-SUPPLIED JAVASCRIPT FILES
When a page is being loaded, the directory _~/.local/share/chorizo/user-scripts_
will be scanned and each file in it ending with *.js* will be run as a
JavaScript file in the context of said page.

*chorizo* comes with the following scripts:

*hints.js*
	Press *f* (open link in current window) or *F* (open in new window) to
	activate link hints. After typing the characters for one of them, press
	*Enter* to confirm. Press *Escape* to abort.

*privacy-redirect.js*
	Redirects YouTube, Reddit, etc to privacy respecting alternatives.

*darkreader.js*
	See https://darkreader.org.

Those bundled scripts are automatically installed on *make install*. To use
them, though, make sure to link them to the directory mentioned above.

# USER-SUPPLIED CSS FILES
User supplied CSS files will be scanned for from
_~/.local/share/chorizo/user-styles_, and be applied every time a page
loads. The rules in these files override any rules provided by the website.

# WEB EXTENSIONS
On startup, WebKit checks _~/.local/share/chorizo/web-extensions_ for any *.so*
files. See
<http://blogs.igalia.com/carlosgc/2013/09/10/webkit2gtk-web-process-extensions/>
this blog post for further information on these extensions.

*chorizo* comes with the following extensions:

*we_adblock.so*
	Generic adblock. Reads patterns from the file _~/.config/chorizo/adblock_. Each
	line can contain a regular expression. These expressions match
	case-insensitive and partially, i.e.*\*foo.\** is the same as *.\*FOO.\** and
	you can use anchors like *^https?://...*. Please refer to
	https://developer.gnome.org/glib/stable/glib-regex-syntax.html the GLib
	reference for more details. Lines starting with "#" are ignored.

	Those bundled web extensions are automatically compiled when you run *make*
	and installed on *make install*. To use them, though, make sure to link them
	to the directory mentioned above.

# TRUSTED CERTIFICATES
By default, *chorizo* trusts whatever CAs are trusted by WebKit. If you wish to
trust additional certificates, such as self-signed certificates, the first thing
you should do is try to add the appropriate CAs to your system-wide store.

If you wish to add simple exceptions, you can grab the certificate and store it
in the directory _~/.local/share/chorizo/certs_. The filename must be equal to
the hostname:

	$ echo | openssl s_client -connect foo.de:443 | openssl x509 >foo.de

This tells *chorizo* to trust the given certificate when connecting to host
*foo.de*.

Note: This is NOT equal to certificate pinning. WebKit ignores user-specified
certificates if the server's certificate can be validated by any system-wide CA.

# SEE ALSO
*chorizo*(1), *chorizo-config*(5)
