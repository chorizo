extern int downloads;
gboolean quit_if_nothing_active(void);
int def_key(char *, unsigned int);
void download_start(WebKitWebView *, WebKitDownload *, gpointer);
void downloadmanager_setup(void);
void downloadmanager_show(void);
