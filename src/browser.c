#include <fcntl.h>
#include <sys/stat.h>
#include <webkit2/webkit2.h>

#include "downloads.h"

void client_destroy(GtkWidget *, gpointer);
WebKitWebView *client_new(const gchar *, WebKitWebView *, gboolean, gboolean);
WebKitWebView *client_new_request(WebKitWebView *, WebKitNavigationAction *,
                                  gpointer);
void cooperation_setup(void);
void changed_load_progress(GObject *, GParamSpec *, gpointer);
void changed_favicon(GObject *, GParamSpec *, gpointer);
void changed_title(GObject *, GParamSpec *, gpointer);
void changed_uri(GObject *, GParamSpec *, gpointer);
gboolean crashed_web_view(WebKitWebView *, gpointer);
gboolean decide_policy(WebKitWebView *, WebKitPolicyDecision *,
                       WebKitPolicyDecisionType, gpointer);
gchar *ensure_uri_scheme(const gchar *);
void grab_environment_configuration(void);
void grab_feeds_finished(GObject *, GAsyncResult *, gpointer);
void hover_web_view(WebKitWebView *, WebKitHitTestResult *, guint, gpointer);
void icon_location(GtkEntry *, GtkEntryIconPosition, GdkEvent *, gpointer);
void init_default_web_context(void);
gboolean key_common(GtkWidget *, GdkEvent *, gpointer);
gboolean key_location(GtkWidget *, GdkEvent *, gpointer);
gboolean key_tablabel(GtkWidget *, GdkEvent *, gpointer);
gboolean key_web_view(GtkWidget *, GdkEvent *, gpointer);
void mainwindow_setup(void);
void mainwindow_title(gint);
void notebook_switch_page(GtkNotebook *, GtkWidget *, guint, gpointer);
gboolean remote_msg(GIOChannel *, GIOCondition, gpointer);
void run_user_scripts(WebKitWebView *);
void search(gpointer, gint);
void show_web_view(WebKitWebView *, gpointer);
void trust_user_certs(WebKitWebContext *);
GKeyFile *get_ini(void);
GKeyFile *config;

struct Client {
    GtkWidget *imgbutton;
    GtkWidget *jsbutton;
    GtkWidget *location;
    GtkWidget *tabicon;
    GtkWidget *tablabel;
    GtkWidget *vbox;
    GtkWidget *web_view;
    WebKitSettings *settings;
    gboolean focus_new_tab;
    gchar *external_handler_uri;
    gchar *feed_html;
    gchar *hover_uri;
};

struct Configuration {
    gdouble zoom_level;
    WebKitCookieAcceptPolicy cookie_policy;
    gboolean cooperative_alone;
    gboolean cooperative_instances;
    gboolean images_enabled;
    gboolean javascript_enabled;
    gboolean private;
    gboolean spellcheck_enabled;
    gchar *default_uri;
    gchar *home_uri;
    gchar *search_engine;
    gchar *spellcheck_language;
    gint scroll_lines;
    gint tab_width_chars;
} cfg;

struct MainWindow {
    GtkWidget *win;
    GtkWidget *notebook;
} mw;

gint clients = 0;
struct Client **client_arr;

int cooperative_pipe_fp = 0;
gchar *search_text;

void
togglejs(GtkButton *jsbutton, gpointer data) {
    struct Client *c = (struct Client *)data;
    webkit_settings_set_enable_javascript(
        c->settings, !webkit_settings_get_enable_javascript(c->settings));
    webkit_web_view_set_settings(WEBKIT_WEB_VIEW(c->web_view), c->settings);
}

void
toggleimg(GtkButton *imgbutton, gpointer data) {
    struct Client *c = (struct Client *)data;
    webkit_settings_set_auto_load_images(
        c->settings, !webkit_settings_get_auto_load_images(c->settings));
    webkit_web_view_set_settings(WEBKIT_WEB_VIEW(c->web_view), c->settings);
}

void
client_destroy(GtkWidget *widget, gpointer data) {
    struct Client *c = (struct Client *)data;
    gint idx;

    g_signal_handlers_disconnect_by_func(G_OBJECT(c->web_view),
                                         changed_load_progress, c);

    idx = gtk_notebook_page_num(GTK_NOTEBOOK(mw.notebook), c->vbox);
    if (idx == -1)
        fprintf(stderr, __NAME__ ": Tab index was -1, bamboozled\n");
    else
        gtk_notebook_remove_page(GTK_NOTEBOOK(mw.notebook), idx);

    free(c);
    clients--;

    quit_if_nothing_active();
}

void
set_uri(const char *uri, struct Client *c) {
    if (!gtk_widget_is_focus(c->location) && uri != NULL)
        gtk_entry_set_text(GTK_ENTRY(c->location), uri);
}

WebKitWebView *
client_new(const gchar *uri, WebKitWebView *related_wv, gboolean show,
           gboolean focus_tab) {
    struct Client *c;
    gchar *f;
    GtkWidget *evbox, *tabbox;

    if (uri != NULL && cfg.cooperative_instances && !cfg.cooperative_alone) {
        f = ensure_uri_scheme(uri);
        write(cooperative_pipe_fp, f, strlen(f));
        write(cooperative_pipe_fp, "\n", 1);
        g_free(f);
        return NULL;
    }

    c = calloc(1, sizeof(struct Client));
    if (!c) {
        fprintf(stderr, __NAME__ ": fatal: calloc failed\n");
        exit(EXIT_FAILURE);
    }

    c->focus_new_tab = focus_tab;

    if (related_wv == NULL) {
        WebKitUserContentManager *ucm = webkit_user_content_manager_new();
        WebKitUserScript *wkscript;
        WebKitUserStyleSheet *wkstyle;
        gchar *path = NULL, *source, *base;
        const gchar *entry = NULL;
        GDir *dir = NULL;

        base = g_build_filename(g_get_user_data_dir(), __NAME__, "user-scripts",
                                NULL);
        dir = g_dir_open(base, 0, NULL);
        if (dir != NULL) {
            while ((entry = g_dir_read_name(dir)) != NULL) {
                path = g_build_filename(base, entry, NULL);
                if (g_str_has_suffix(path, ".js")) {
                    g_file_get_contents(path, &source, NULL, NULL);
                    wkscript = webkit_user_script_new(
                        source, WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
                        WEBKIT_USER_SCRIPT_INJECT_AT_DOCUMENT_START, NULL,
                        NULL);
                    webkit_user_content_manager_add_script(ucm, wkscript);
                    webkit_user_script_unref(wkscript);
                }
                g_free(path);
                g_free(source);
            }
            g_dir_close(dir);
        }

        base = g_build_filename(g_get_user_data_dir(), __NAME__, "user-styles",
                                NULL);
        dir = g_dir_open(base, 0, NULL);
        if (dir != NULL) {
            while ((entry = g_dir_read_name(dir)) != NULL) {
                path = g_build_filename(base, entry, NULL);
                if (g_str_has_suffix(path, ".css")) {
                    g_file_get_contents(path, &source, NULL, NULL);
                    wkstyle = webkit_user_style_sheet_new(
                        source, WEBKIT_USER_CONTENT_INJECT_ALL_FRAMES,
                        WEBKIT_USER_STYLE_LEVEL_USER, NULL, NULL);
                    webkit_user_content_manager_add_style_sheet(ucm, wkstyle);
                    webkit_user_style_sheet_unref(wkstyle);
                }
                g_free(path);
                g_free(source);
            }
            g_dir_close(dir);
        }

        g_free(base);

        c->web_view = webkit_web_view_new_with_user_content_manager(ucm);
    } else {
        c->web_view = webkit_web_view_new_with_related_view(related_wv);
    }

    // Get settings
    c->settings = webkit_web_view_get_settings(WEBKIT_WEB_VIEW(c->web_view));
    webkit_settings_set_enable_javascript(c->settings, cfg.javascript_enabled);
    webkit_settings_set_auto_load_images(c->settings, cfg.images_enabled);

    GKeyFile *config = get_ini();
    char *val;
    val = g_key_file_get_string(config, "ui", "font_family_default", NULL);
    if (val != NULL)
        webkit_settings_set_default_font_family(c->settings, val);

    val = g_key_file_get_string(config, "ui", "font_family_default_monospace",
                                NULL);
    if (val != NULL)
        webkit_settings_set_monospace_font_family(c->settings, val);

    val = g_key_file_get_string(config, "ui", "font_family_default_sans_serif",
                                NULL);
    if (val != NULL)
        webkit_settings_set_sans_serif_font_family(c->settings, val);

    val =
        g_key_file_get_string(config, "ui", "font_family_default_serif", NULL);
    if (val != NULL)
        webkit_settings_set_serif_font_family(c->settings, val);

    val = g_key_file_get_string(config, "browser", "user_agent", NULL);
    if (val != NULL)
        g_object_set(c->settings, "user-agent", val, NULL);

    int val2;
    val2 = g_key_file_get_integer(config, "ui", "font_size_default", NULL);
    if (val2)
        webkit_settings_set_default_font_size(c->settings, val2);

    val2 = g_key_file_get_integer(config, "ui", "font_size_default_monospace",
                                  NULL);
    if (val2)
        webkit_settings_set_default_monospace_font_size(c->settings, val2);

    gboolean val3;
    val3 = g_key_file_get_boolean(config, "browser", "console_to_stdout", NULL);
    if (val3)
        webkit_settings_set_enable_write_console_messages_to_stdout(c->settings,
                                                                    val3);

    val3 = g_key_file_get_boolean(config, "browser", "webgl_enable", NULL);
    if (!val3)
        webkit_settings_set_enable_webgl(c->settings, val3);

    val3 = g_key_file_get_boolean(config, "browser",
                                  "javascript_can_access_clipboard", NULL);
    if (val3)
        webkit_settings_set_javascript_can_access_clipboard(c->settings, val3);

    double val4;
    val4 = g_key_file_get_double(config, "ui", "zoom_level", NULL);
    if (val4)
        webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(c->web_view), val4);

    webkit_settings_set_enable_developer_extras(c->settings, TRUE);

    g_signal_connect(G_OBJECT(c->web_view), "notify::favicon",
                     G_CALLBACK(changed_favicon), c);
    g_signal_connect(G_OBJECT(c->web_view), "notify::title",
                     G_CALLBACK(changed_title), c);
    g_signal_connect(G_OBJECT(c->web_view), "notify::uri",
                     G_CALLBACK(changed_uri), c);
    g_signal_connect(G_OBJECT(c->web_view), "notify::estimated-load-progress",
                     G_CALLBACK(changed_load_progress), c);
    g_signal_connect(G_OBJECT(c->web_view), "create",
                     G_CALLBACK(client_new_request), NULL);
    g_signal_connect(G_OBJECT(c->web_view), "close", G_CALLBACK(client_destroy),
                     c);
    g_signal_connect(G_OBJECT(c->web_view), "decide-policy",
                     G_CALLBACK(decide_policy), NULL);
    g_signal_connect(G_OBJECT(c->web_view), "key-press-event",
                     G_CALLBACK(key_web_view), c);
    g_signal_connect(G_OBJECT(c->web_view), "button-release-event",
                     G_CALLBACK(key_web_view), c);
    g_signal_connect(G_OBJECT(c->web_view), "scroll-event",
                     G_CALLBACK(key_web_view), c);
    g_signal_connect(G_OBJECT(c->web_view), "mouse-target-changed",
                     G_CALLBACK(hover_web_view), c);
    g_signal_connect(G_OBJECT(c->web_view), "web-process-crashed",
                     G_CALLBACK(crashed_web_view), c);

    GtkWidget *locbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

    c->jsbutton = gtk_toggle_button_new_with_label("JS");
    gtk_widget_set_tooltip_text(c->jsbutton, "Toggle JavaScript execution");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(c->jsbutton),
                                 cfg.javascript_enabled);
    g_signal_connect(G_OBJECT(c->jsbutton), "toggled", G_CALLBACK(togglejs), c);

    c->imgbutton = gtk_toggle_button_new_with_label("IMG");
    gtk_widget_set_tooltip_text(c->imgbutton, "Toggle image loading");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(c->imgbutton),
                                 cfg.images_enabled);
    g_signal_connect(G_OBJECT(c->imgbutton), "toggled", G_CALLBACK(toggleimg),
                     c);

    c->location = gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(locbox), c->location, TRUE, TRUE, 0);

    if (cfg.private) {
        GtkWidget *privindicator = gtk_label_new("Private mode");
        gtk_widget_set_tooltip_text(
            privindicator, "You are in private mode. No history, caches, or "
                           "cookies will be saved beyond this session.");
        gtk_box_pack_end(GTK_BOX(locbox), privindicator, FALSE, FALSE, 5);
    }

    gtk_box_pack_start(GTK_BOX(locbox), c->jsbutton, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(locbox), c->imgbutton, FALSE, FALSE, 0);

    g_signal_connect(G_OBJECT(c->location), "key-press-event",
                     G_CALLBACK(key_location), c);
    g_signal_connect(G_OBJECT(c->location), "icon-release",
                     G_CALLBACK(icon_location), c);
    /* XXX This is a workaround. Setting this to NULL (which is done in
     * grab_feeds_finished() if no feed has been detected) adds a little
     * padding left of the text. Not sure why. The point of this call
     * right here is to have that padding right from the start. This
     * avoids a graphical artifact. */
    gtk_entry_set_icon_from_icon_name(GTK_ENTRY(c->location),
                                      GTK_ENTRY_ICON_SECONDARY, NULL);

    c->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(c->vbox), locbox, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(c->vbox), c->web_view, TRUE, TRUE, 0);
    gtk_container_set_focus_child(GTK_CONTAINER(c->vbox), c->web_view);

    c->tabicon =
        gtk_image_new_from_icon_name("text-html", GTK_ICON_SIZE_SMALL_TOOLBAR);

    c->tablabel = gtk_label_new(__NAME__);
    gtk_label_set_ellipsize(GTK_LABEL(c->tablabel), PANGO_ELLIPSIZE_END);
    gtk_label_set_width_chars(GTK_LABEL(c->tablabel), cfg.tab_width_chars);
    gtk_widget_set_has_tooltip(c->tablabel, TRUE);

    /* XXX I don't own a HiDPI screen, so I don't know if scale_factor
     * does the right thing. */
    tabbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,
                         5 * gtk_widget_get_scale_factor(mw.win));
    gtk_box_pack_start(GTK_BOX(tabbox), c->tabicon, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(tabbox), c->tablabel, TRUE, TRUE, 0);

    evbox = gtk_event_box_new();
    gtk_container_add(GTK_CONTAINER(evbox), tabbox);
    g_signal_connect(G_OBJECT(evbox), "button-release-event",
                     G_CALLBACK(key_tablabel), c);

    gtk_widget_add_events(evbox, GDK_SCROLL_MASK);
    g_signal_connect(G_OBJECT(evbox), "scroll-event", G_CALLBACK(key_tablabel),
                     c);

    // For easy access, store a reference to our label.
    g_object_set_data(G_OBJECT(evbox), "chorizo-tab-label", c->tablabel);

    /* This only shows the event box and the label inside, nothing else.
     * Needed because the evbox/label is "internal" to the notebook and
     * not part of the normal "widget tree" (IIUC). */
    gtk_widget_show_all(evbox);

    int page = gtk_notebook_get_current_page(GTK_NOTEBOOK(mw.notebook)) + 1;
    gtk_notebook_insert_page(GTK_NOTEBOOK(mw.notebook), c->vbox, evbox, page);
    gtk_notebook_set_tab_reorderable(GTK_NOTEBOOK(mw.notebook), c->vbox, TRUE);

    if (show)
        show_web_view(NULL, c);
    else
        g_signal_connect(G_OBJECT(c->web_view), "ready-to-show",
                         G_CALLBACK(show_web_view), c);

    if (uri != NULL) {
        f = ensure_uri_scheme(uri);
        webkit_web_view_load_uri(WEBKIT_WEB_VIEW(c->web_view), f);
        g_free(f);
    }

    set_uri(uri, c);

    clients++;
    client_arr = realloc(client_arr, (clients + 1) * sizeof(client_arr[0]));
    client_arr[clients] = c;

    return WEBKIT_WEB_VIEW(c->web_view);
}

WebKitWebView *
client_new_request(WebKitWebView *web_view,
                   WebKitNavigationAction *navigation_action, gpointer data) {
    return client_new(NULL, web_view, FALSE, FALSE);
}

void
cooperation_setup(void) {
    GIOChannel *towatch;
    gchar *fifofilename, *fifopath;

    gchar *priv = (cfg.private) ? "-private" : "";
    fifofilename = g_strdup_printf("%s%s%s-%s", __NAME__, priv, ".fifo",
                                   g_getenv(__NAME_UPPERCASE__ "_FIFO_SUFFIX"));
    fifopath = g_build_filename(g_get_user_runtime_dir(), fifofilename, NULL);
    g_free(fifofilename);

    if (!g_file_test(fifopath, G_FILE_TEST_EXISTS))
        mkfifo(fifopath, 0600);

    cooperative_pipe_fp = open(fifopath, O_WRONLY | O_NONBLOCK);
    if (!cooperative_pipe_fp) {
        fprintf(stderr, __NAME__ ": Can't open FIFO at all.\n");
    } else {
        if (write(cooperative_pipe_fp, "", 0) == -1) {
            /* Could not do an empty write to the FIFO which means there's
             * no one listening. */
            close(cooperative_pipe_fp);
            towatch = g_io_channel_new_file(fifopath, "r+", NULL);
            g_io_add_watch(towatch, G_IO_IN, (GIOFunc)remote_msg, NULL);
        } else {
            cfg.cooperative_alone = FALSE;
        }
    }

    g_free(fifopath);
}

void
changed_load_progress(GObject *obj, GParamSpec *pspec, gpointer data) {
    struct Client *c = (struct Client *)data;
    gdouble p;
    gchar *grab_feeds =
        "a = document.querySelectorAll('"
        "    html > head > "
        "link[rel=\"alternate\"][href][type=\"application/atom+xml\"],"
        "    html > head > "
        "link[rel=\"alternate\"][href][type=\"application/rss+xml\"]"
        "');"
        "if (a.length == 0)"
        "    null;"
        "else {"
        "    out = '';"
        "    for (i = 0; i < a.length; i++) {"
        "        url = encodeURIComponent(a[i].href);"
        "        if ('title' in a[i] && a[i].title != '')"
        "            title = encodeURIComponent(a[i].title);"
        "        else"
        "            title = url;"
        "        out += '<li><a href=\"' + url + '\">' + title + "
        "'</a></li>';"
        "    }"
        "    out;"
        "}";

    p = webkit_web_view_get_estimated_load_progress(
        WEBKIT_WEB_VIEW(c->web_view));
    if (p == 1) {
        p = 0;

        /* The page has loaded fully. We now run the short JavaScript
         * snippet above that operates on the DOM. It tries to grab all
         * occurences of <link rel="alternate" ...>, i.e. RSS/Atom feed
         * references. */
        webkit_web_view_run_javascript(WEBKIT_WEB_VIEW(c->web_view), grab_feeds,
                                       NULL, grab_feeds_finished, c);
    }
    gtk_entry_set_progress_fraction(GTK_ENTRY(c->location), p);
}

void
changed_favicon(GObject *obj, GParamSpec *pspec, gpointer data) {
    struct Client *c = (struct Client *)data;
    cairo_surface_t *f;
    int w, h, w_should, h_should;
    GdkPixbuf *pb, *pb_scaled;

    f = webkit_web_view_get_favicon(WEBKIT_WEB_VIEW(c->web_view));
    if (f == NULL) {
        gtk_image_set_from_icon_name(GTK_IMAGE(c->tabicon), "text-html",
                                     GTK_ICON_SIZE_SMALL_TOOLBAR);
    } else {
        w = cairo_image_surface_get_width(f);
        h = cairo_image_surface_get_height(f);
        pb = gdk_pixbuf_get_from_surface(f, 0, 0, w, h);
        if (pb != NULL) {
            w_should = 16 * gtk_widget_get_scale_factor(c->tabicon);
            h_should = 16 * gtk_widget_get_scale_factor(c->tabicon);
            pb_scaled = gdk_pixbuf_scale_simple(pb, w_should, h_should,
                                                GDK_INTERP_BILINEAR);
            gtk_image_set_from_pixbuf(GTK_IMAGE(c->tabicon), pb_scaled);

            g_object_unref(pb_scaled);
            g_object_unref(pb);
        }
    }
}

void
changed_title(GObject *obj, GParamSpec *pspec, gpointer data) {
    const gchar *t, *u;
    struct Client *c = (struct Client *)data;

    u = webkit_web_view_get_uri(WEBKIT_WEB_VIEW(c->web_view));
    t = webkit_web_view_get_title(WEBKIT_WEB_VIEW(c->web_view));

    u = u == NULL ? __NAME__ : u;
    u = u[0] == 0 ? __NAME__ : u;

    t = t == NULL ? u : t;
    t = t[0] == 0 ? u : t;

    gchar *name = malloc(strlen(t) + 4);
    gboolean mute = webkit_web_view_get_is_muted(WEBKIT_WEB_VIEW(c->web_view));
    gchar *muted = (mute) ? "[m] " : "";
    sprintf(name, "%s%s", muted, t);
    gtk_label_set_text(GTK_LABEL(c->tablabel), name);
    g_free(name);

    gtk_widget_set_tooltip_text(c->tablabel, t);
    mainwindow_title(gtk_notebook_get_current_page(GTK_NOTEBOOK(mw.notebook)));
}

void
changed_uri(GObject *obj, GParamSpec *pspec, gpointer data) {
    const gchar *t;
    struct Client *c = (struct Client *)data;
    FILE *fp;

    t = webkit_web_view_get_uri(WEBKIT_WEB_VIEW(c->web_view));

    /* When a web process crashes, we get a "notify::uri" signal, but we
     * can no longer read a meaningful URI. It's just an empty string
     * now. Not updating the location bar in this scenario is important,
     * because we would override the "WEB PROCESS CRASHED" message. */
    if (t != NULL && strlen(t) > 0) {
        set_uri(t, c);

        gchar *history_file =
            g_key_file_get_string(config, "browser", "history_file", NULL);

        if (history_file != NULL && !cfg.private) {
            fp = fopen(history_file, "a");
            if (fp != NULL) {
                fprintf(fp, "%s\n", t);
                fclose(fp);
            } else {
                perror(__NAME__ ": Error opening history file");
            }
        }
    }
}

gboolean
crashed_web_view(WebKitWebView *web_view, gpointer data) {
    gchar *t;
    struct Client *c = (struct Client *)data;

    t = g_strdup_printf("WEB PROCESS CRASHED: %s",
                        webkit_web_view_get_uri(WEBKIT_WEB_VIEW(web_view)));
    gtk_entry_set_text(GTK_ENTRY(c->location), t);
    g_free(t);

    return TRUE;
}

gboolean
decide_policy(WebKitWebView *web_view, WebKitPolicyDecision *decision,
              WebKitPolicyDecisionType type, gpointer data) {
    WebKitResponsePolicyDecision *r;

    switch (type) {
    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
        r = WEBKIT_RESPONSE_POLICY_DECISION(decision);
        if (!webkit_response_policy_decision_is_mime_type_supported(r))
            webkit_policy_decision_download(decision);
        else
            webkit_policy_decision_use(decision);
        break;
    default:
        // Use whatever default there is.
        return FALSE;
    }
    return TRUE;
}

gchar *
ensure_uri_scheme(const gchar *t) {
    gchar *f, *fabs;

    f = g_ascii_strdown(t, -1);
    if (!g_str_has_prefix(f, "http:") && !g_str_has_prefix(f, "https:") &&
        !g_str_has_prefix(f, "file:") && !g_str_has_prefix(f, "about:") &&
        !g_str_has_prefix(f, "data:") && !g_str_has_prefix(f, "webkit:")) {
        g_free(f);
        fabs = realpath(t, NULL);
        if (fabs != NULL) {
            f = g_strdup_printf("file://%s", fabs);
            free(fabs);
        } else {
            f = g_strdup_printf("http://%s", t);
        }
        return f;
    } else
        return g_strdup(t);
}

void
get_config(void) {
    cfg.cooperative_alone = TRUE;
    cfg.cooperative_instances = TRUE;

    config = get_ini();

    cfg.home_uri = g_key_file_get_string(config, "browser", "homepage", NULL);
    cfg.home_uri = (cfg.home_uri) ? cfg.home_uri : "about:blank";

    cfg.javascript_enabled =
        g_key_file_get_boolean(config, "browser", "javascript_enabled", NULL);
    cfg.javascript_enabled =
        (cfg.javascript_enabled) ? cfg.javascript_enabled : TRUE;

    cfg.images_enabled =
        g_key_file_get_boolean(config, "browser", "images_enabled", NULL);
    cfg.images_enabled = (cfg.images_enabled) ? cfg.images_enabled : TRUE;

    char *input_cookie_policy =
        g_key_file_get_string(config, "browser", "cookie_policy", NULL);
    cfg.cookie_policy = WEBKIT_COOKIE_POLICY_ACCEPT_NO_THIRD_PARTY;
    if (input_cookie_policy) {
        if (strcmp(input_cookie_policy, "all") == 0) {
            cfg.cookie_policy = WEBKIT_COOKIE_POLICY_ACCEPT_ALWAYS;
        } else if (strcmp(input_cookie_policy, "none") == 0) {
            cfg.cookie_policy = WEBKIT_COOKIE_POLICY_ACCEPT_NEVER;
        }
    }

    cfg.default_uri = g_key_file_get_string(config, "ui", "default_uri", NULL);
    cfg.default_uri = (cfg.default_uri) ? cfg.default_uri : "https://";

    cfg.tab_width_chars =
        g_key_file_get_integer(config, "ui", "tab_width", NULL);
    cfg.tab_width_chars = (cfg.tab_width_chars) ? cfg.tab_width_chars : 20;

    cfg.search_engine =
        g_key_file_get_string(config, "ui", "search_engine", NULL);
    cfg.search_engine =
        (cfg.search_engine) ? cfg.search_engine : "https://duckduckgo.com?q=";

    cfg.spellcheck_enabled =
        g_key_file_get_boolean(config, "ui", "spellcheck_enabled", NULL);
    cfg.spellcheck_enabled =
        (cfg.spellcheck_enabled) ? cfg.spellcheck_enabled : FALSE;

    cfg.spellcheck_language =
        g_key_file_get_string(config, "ui", "spellcheck_language", NULL);
    cfg.spellcheck_language =
        (cfg.spellcheck_language) ? cfg.spellcheck_language : "en_US";

    cfg.scroll_lines =
        g_key_file_get_integer(config, "ui", "scroll_lines", NULL);
    cfg.scroll_lines = (cfg.scroll_lines) ? cfg.scroll_lines : 3;
}

void
grab_feeds_finished(GObject *object, GAsyncResult *result, gpointer data) {
    struct Client *c = (struct Client *)data;
    WebKitJavascriptResult *js_result;
    JSCValue *value;
    JSCException *exception;
    GError *err = NULL;
    gchar *str_value;

    g_free(c->feed_html);
    c->feed_html = NULL;

    /* This was taken almost verbatim from the example in WebKit's
     * documentation:
     *
     * https://webkitgtk.org/reference/webkit2gtk/stable/WebKitWebView.html
     */

    js_result = webkit_web_view_run_javascript_finish(WEBKIT_WEB_VIEW(object),
                                                      result, &err);
    if (!js_result) {
        fprintf(stderr, __NAME__ ": Error running javascript: %s\n",
                err->message);
        g_error_free(err);
        return;
    }

    value = webkit_javascript_result_get_js_value(js_result);
    if (jsc_value_is_string(value)) {
        str_value = jsc_value_to_string(value);
        exception = jsc_context_get_exception(jsc_value_get_context(value));
        if (exception != NULL) {
            fprintf(stderr, __NAME__ ": Error running javascript: %s\n",
                    jsc_exception_get_message(exception));
        } else {
            c->feed_html = str_value;
        }

        gtk_entry_set_icon_from_icon_name(GTK_ENTRY(c->location),
                                          GTK_ENTRY_ICON_SECONDARY,
                                          "application-rss+xml-symbolic");
        gtk_entry_set_icon_activatable(GTK_ENTRY(c->location),
                                       GTK_ENTRY_ICON_SECONDARY, TRUE);
    } else {
        gtk_entry_set_icon_from_icon_name(GTK_ENTRY(c->location),
                                          GTK_ENTRY_ICON_SECONDARY, NULL);
    }

    webkit_javascript_result_unref(js_result);
}

void
hover_web_view(WebKitWebView *web_view, WebKitHitTestResult *ht,
               guint modifiers, gpointer data) {
    struct Client *c = (struct Client *)data;
    const char *to_show;

    g_free(c->hover_uri);

    if (webkit_hit_test_result_context_is_link(ht)) {
        to_show = webkit_hit_test_result_get_link_uri(ht);
        c->hover_uri = g_strdup(to_show);
    } else {
        to_show = webkit_web_view_get_uri(WEBKIT_WEB_VIEW(c->web_view));
        c->hover_uri = NULL;
    }

    if (!gtk_widget_is_focus(c->location))
        set_uri(to_show, c);
}

void
icon_location(GtkEntry *entry, GtkEntryIconPosition icon_pos, GdkEvent *event,
              gpointer data) {
    struct Client *c = (struct Client *)data;
    gchar *d;
    gchar *data_template = "data:text/html,"
                           "<!DOCTYPE html>"
                           "<html>"
                           "    <head>"
                           "        <meta charset=\"UTF-8\">"
                           "        <title>Feeds</title>"
                           "    </head>"
                           "    <body>"
                           "        <p>Feeds found on this page:</p>"
                           "        <ul>"
                           "        %s"
                           "        </ul>"
                           "    </body>"
                           "</html>";

    if (c->feed_html != NULL) {
        /* What we're actually trying to do is show a simple HTML page
         * that lists all the feeds on the current page. The function
         * webkit_web_view_load_html() looks like the proper way to do
         * that. Sad thing is, it doesn't create a history entry, but
         * instead simply replaces the content of the current page. This
         * is not what we want.
         *
         * RFC 2397 [0] defines the data URI scheme [1]. We abuse this
         * mechanism to show my custom HTML snippet *and* create a
         * history entry.
         *
         * [0]: https://tools.ietf.org/html/rfc2397
         * [1]: https://en.wikipedia.org/wiki/Data_URI_scheme */
        d = g_strdup_printf(data_template, c->feed_html);
        webkit_web_view_load_uri(WEBKIT_WEB_VIEW(c->web_view), d);
        g_free(d);
    }
}

void
init_default_web_context(void) {
    gchar *p;
    WebKitWebContext *wc;
    WebKitCookieManager *cm;

    wc = (cfg.private) ? webkit_web_context_new_ephemeral()
                       : webkit_web_context_get_default();

    p = g_build_filename(g_get_user_config_dir(), __NAME__, "adblock", NULL);
    webkit_web_context_set_sandbox_enabled(wc, TRUE);
    webkit_web_context_add_path_to_sandbox(wc, p, TRUE);
    g_free(p);

    WebKitProcessModel model =
        WEBKIT_PROCESS_MODEL_MULTIPLE_SECONDARY_PROCESSES;
    webkit_web_context_set_process_model(wc, model);

    p = g_build_filename(g_get_user_data_dir(), __NAME__, "web-extensions",
                         NULL);
    webkit_web_context_set_web_extensions_directory(wc, p);
    g_free(p);

    char *xdg_down = getenv("XDG_DOWNLOAD_DIR");
    g_signal_connect(G_OBJECT(wc), "download-started",
                     G_CALLBACK(download_start),
                     (xdg_down) ? xdg_down : "/var/tmp");

    trust_user_certs(wc);

    cm = webkit_web_context_get_cookie_manager(wc);
    webkit_cookie_manager_set_accept_policy(cm, cfg.cookie_policy);

    if (!cfg.private) {
        webkit_web_context_set_favicon_database_directory(wc, NULL);

        gchar *fname = g_build_filename("/", g_get_user_data_dir(), __NAME__,
                                        "cookies.db", NULL);
        WebKitCookiePersistentStorage type =
            WEBKIT_COOKIE_PERSISTENT_STORAGE_SQLITE;
        webkit_cookie_manager_set_persistent_storage(cm, fname, type);
        g_free(fname);
    }

    const gchar *const languages[2] = {(const gchar *)cfg.spellcheck_language,
                                       NULL};
    webkit_web_context_set_spell_checking_languages(wc, languages);
    webkit_web_context_set_spell_checking_enabled(wc, cfg.spellcheck_enabled);
}

void
search(gpointer data, gint direction) {
    struct Client *c = (struct Client *)data;
    WebKitWebView *web_view = WEBKIT_WEB_VIEW(c->web_view);
    WebKitFindController *fc = webkit_web_view_get_find_controller(web_view);

    if (search_text == NULL)
        return;

    switch (direction) {
    case 0:
        webkit_find_controller_search(fc, search_text,
                                      WEBKIT_FIND_OPTIONS_CASE_INSENSITIVE |
                                          WEBKIT_FIND_OPTIONS_WRAP_AROUND,
                                      G_MAXUINT);
        break;
    case 1:
        webkit_find_controller_search_next(fc);
        break;
    case -1:
        webkit_find_controller_search_previous(fc);
        break;
    case 2:
        webkit_find_controller_search_finish(fc);
        break;
    }
}

void
search_init(struct Client *c, int direction) {
    gtk_widget_grab_focus(c->location);
    const gchar *contents = gtk_entry_get_text(GTK_ENTRY(c->location));
    if (strcspn(contents, "s/")) {
        gtk_entry_set_text(GTK_ENTRY(c->location), "s/");
        gtk_editable_set_position(GTK_EDITABLE(c->location), -1);
    } else {
        search(c, 0);
        search(c, -1);
        search(c, direction);
    }
}

int
def_key(char *key, unsigned int def) {
    char *conf = g_key_file_get_string(config, "keybindings", key, NULL);
    return (conf) ? gdk_keyval_from_name((conf) ? conf : NULL) : def;
}

gboolean
key_common(GtkWidget *widget, GdkEvent *event, gpointer data) {
    struct Client *c = (struct Client *)data;
    gdouble now;
    gchar *f;

    if (event->type == GDK_KEY_PRESS) {
        if (((GdkEventKey *)event)->state & GDK_CONTROL_MASK) {
            const char *uri =
                webkit_web_view_get_uri(WEBKIT_WEB_VIEW(c->web_view));
            int key = ((GdkEventKey *)event)->keyval;
            if (def_key("download_manager", GDK_KEY_y) == key) {
                downloadmanager_show();
                return TRUE;
            } else if (def_key("history_back", GDK_KEY_h) == key) {
                webkit_web_view_go_back(WEBKIT_WEB_VIEW(c->web_view));
                return TRUE;
            } else if (def_key("history_forwards", GDK_KEY_l) == key) {
                webkit_web_view_go_forward(WEBKIT_WEB_VIEW(c->web_view));
                return TRUE;
            } else if (def_key("location", GDK_KEY_t) == key) {
                gtk_widget_grab_focus(c->location);
                const char *goal = (strcmp(cfg.home_uri, uri) == 0 || !uri)
                                       ? cfg.default_uri
                                       : uri;
                gtk_entry_set_text(GTK_ENTRY(c->location), goal);
                gtk_editable_set_position(GTK_EDITABLE(c->location), -1);
                return TRUE;
            } else if (def_key("print", GDK_KEY_Print) == key) {
                WebKitPrintOperation *operation =
                    webkit_print_operation_new(WEBKIT_WEB_VIEW(c->web_view));
                GtkWidget *toplevel = gtk_widget_get_toplevel(mw.win);
                webkit_print_operation_run_dialog(operation,
                                                  GTK_WINDOW(toplevel));
                return TRUE;
            } else if (def_key("quit", GDK_KEY_g) == key) {
                search(c, 2);
                gtk_widget_grab_focus(c->web_view);
                gtk_entry_set_text(GTK_ENTRY(c->location), uri);
                gtk_editable_set_position(GTK_EDITABLE(c->location), -1);
                webkit_web_view_run_javascript(
                    WEBKIT_WEB_VIEW(c->web_view),
                    "window.getSelection().removeAllRanges();"
                    "document.activeElement.blur();",
                    NULL, NULL, c);
                return TRUE;
            } else if (def_key("reload", GDK_KEY_e) == key) {
                webkit_web_view_reload_bypass_cache(
                    WEBKIT_WEB_VIEW(c->web_view));
                return TRUE;
            } else if (def_key("scroll_line_down", GDK_KEY_j) == key) {
                for (int i = 0; i <= cfg.scroll_lines - 1; i++) {
                    event->key.keyval = GDK_KEY_Down;
                    gdk_event_put(event);
                }
                return TRUE;
            } else if (def_key("scroll_line_up", GDK_KEY_k) == key) {
                for (int i = 0; i <= cfg.scroll_lines - 1; i++) {
                    event->key.keyval = GDK_KEY_Up;
                    gdk_event_put(event);
                }
                return TRUE;
            } else if (def_key("scroll_page_down", GDK_KEY_f) == key) {
                event->key.keyval = GDK_KEY_Page_Down;
                gdk_event_put(event);
                return TRUE;
            } else if (def_key("scroll_page_up", GDK_KEY_b) == key) {
                event->key.keyval = GDK_KEY_Page_Up;
                gdk_event_put(event);
                return TRUE;
            } else if (def_key("search_forwards", GDK_KEY_s) == key) {
                search_init(c, 1);
                return TRUE;
            } else if (def_key("search_backwards", GDK_KEY_r) == key) {
                search_init(c, -1);
                return TRUE;
            } else if (def_key("tab_close", GDK_KEY_q) == key) {
                client_destroy(NULL, c);
                return TRUE;
            } else if (def_key("tab_switch_1", GDK_KEY_1) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 0);
                return TRUE;
            } else if (def_key("tab_switch_2", GDK_KEY_2) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 1);
                return TRUE;
            } else if (def_key("tab_switch_3", GDK_KEY_3) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 2);
                return TRUE;
            } else if (def_key("tab_switch_4", GDK_KEY_4) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 3);
                return TRUE;
            } else if (def_key("tab_switch_5", GDK_KEY_5) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 4);
                return TRUE;
            } else if (def_key("tab_switch_6", GDK_KEY_6) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 5);
                return TRUE;
            } else if (def_key("tab_switch_7", GDK_KEY_7) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 6);
                return TRUE;
            } else if (def_key("tab_switch_8", GDK_KEY_8) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 7);
                return TRUE;
            } else if (def_key("tab_switch_9", GDK_KEY_9) == key) {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), 8);
                return TRUE;
            } else if (def_key("tab_previous", GDK_KEY_u) == key) {
                gtk_notebook_prev_page(GTK_NOTEBOOK(mw.notebook));
                return TRUE;
            } else if (def_key("tab_mute", GDK_KEY_m) == key) {
                gboolean muted =
                    webkit_web_view_get_is_muted(WEBKIT_WEB_VIEW(c->web_view));
                webkit_web_view_set_is_muted(WEBKIT_WEB_VIEW(c->web_view),
                                             !muted);
                changed_title(G_OBJECT(c->web_view), NULL, c);
                return TRUE;
            } else if (def_key("tab_new", GDK_KEY_w) == key) {
                f = ensure_uri_scheme(cfg.home_uri);
                client_new(f, NULL, TRUE, TRUE);
                g_free(f);
                return TRUE;
            } else if (def_key("tab_next", GDK_KEY_i) == key) {
                gtk_notebook_next_page(GTK_NOTEBOOK(mw.notebook));
                return TRUE;
            } else if (def_key("toggle_js", GDK_KEY_o) == key) {
                gboolean on =
                    webkit_settings_get_enable_javascript(c->settings);
                webkit_settings_set_enable_javascript(c->settings, !on);
                webkit_web_view_set_settings(WEBKIT_WEB_VIEW(c->web_view),
                                             c->settings);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(c->jsbutton),
                                             !on);
            } else if (def_key("toggle_img", -1) == key) {
                gboolean on = webkit_settings_get_auto_load_images(c->settings);
                webkit_settings_set_auto_load_images(c->settings, !on);
                webkit_web_view_set_settings(WEBKIT_WEB_VIEW(c->web_view),
                                             c->settings);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(c->imgbutton),
                                             !on);
            } else if (def_key("web_search", GDK_KEY_d) == key) {
                gtk_widget_grab_focus(c->location);
                gtk_entry_set_text(GTK_ENTRY(c->location), "w/");
                gtk_editable_set_position(GTK_EDITABLE(c->location), -1);
                return TRUE;
            } else if (def_key("zoom_in", GDK_KEY_equal) == key) {
                now = webkit_web_view_get_zoom_level(
                    WEBKIT_WEB_VIEW(c->web_view));
                webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(c->web_view),
                                               now + 0.1);
                return TRUE;
            } else if (def_key("zoom_out", GDK_KEY_minus) == key) {
                now = webkit_web_view_get_zoom_level(
                    WEBKIT_WEB_VIEW(c->web_view));
                webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(c->web_view),
                                               now - 0.1);
                return TRUE;
            } else if (def_key("zoom_reset", GDK_KEY_0) == key) {
                webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(c->web_view),
                                               cfg.zoom_level);
                return TRUE;
            }
        }
    }
    return FALSE;
}

gboolean
key_location(GtkWidget *widget, GdkEvent *event, gpointer data) {
    struct Client *c = (struct Client *)data;
    const gchar *t;
    gchar *f;

    if (key_common(widget, event, data))
        return TRUE;

    if (event->type == GDK_KEY_PRESS) {
        int key = ((GdkEventKey *)event)->keyval;
        if ((GDK_KEY_KP_Enter == key) || (GDK_KEY_Return == key)) {
            gtk_widget_grab_focus(c->web_view);
            t = gtk_entry_get_text(GTK_ENTRY(c->location));
            if (t != NULL && t[0] == 's' && t[1] == '/') {
                if (search_text != NULL)
                    g_free(search_text);
                search_text = g_strdup(t + 2);
                search(c, 0);
            } else if (t != NULL && t[0] == 'w' && t[1] == '/') {
                int len = strlen(cfg.search_engine) + strlen(t) - 2;
                gchar *f = malloc(len + 1);
                snprintf(f, len + 1, "%s%s", cfg.search_engine, t + 2);
                webkit_web_view_load_uri(WEBKIT_WEB_VIEW(c->web_view), f);
                g_free(f);
            } else {
                f = ensure_uri_scheme(t);
                webkit_web_view_load_uri(WEBKIT_WEB_VIEW(c->web_view), f);
                g_free(f);
            }
            return TRUE;
        } else if (GDK_KEY_Escape == key) {
            t = webkit_web_view_get_uri(WEBKIT_WEB_VIEW(c->web_view));
            gtk_entry_set_text(GTK_ENTRY(c->location), (t == NULL) ? "" : t);
            return TRUE;
        }
    }
    return FALSE;
}

gboolean
key_tablabel(GtkWidget *widget, GdkEvent *event, gpointer data) {
    GdkScrollDirection direction;

    if (event->type == GDK_BUTTON_RELEASE) {
        switch (((GdkEventButton *)event)->button) {
        case 2:
            client_destroy(NULL, data);
            return TRUE;
        }
    } else if (event->type == GDK_SCROLL) {
        gdk_event_get_scroll_direction(event, &direction);
        switch (direction) {
        case GDK_SCROLL_UP:
            gtk_notebook_prev_page(GTK_NOTEBOOK(mw.notebook));
            break;
        case GDK_SCROLL_DOWN:
            gtk_notebook_next_page(GTK_NOTEBOOK(mw.notebook));
            break;
        default:
            break;
        }
        return TRUE;
    }
    return FALSE;
}

gboolean
key_web_view(GtkWidget *widget, GdkEvent *event, gpointer data) {
    struct Client *c = (struct Client *)data;
    gdouble dx, dy;
    gfloat z;

    if (key_common(widget, event, data))
        return TRUE;

    if (event->type == GDK_KEY_PRESS) {
        if (((GdkEventKey *)event)->keyval == GDK_KEY_Escape) {
            webkit_web_view_stop_loading(WEBKIT_WEB_VIEW(c->web_view));
            gtk_entry_set_progress_fraction(GTK_ENTRY(c->location), 0);
        }
    } else if (event->type == GDK_BUTTON_RELEASE) {
        GdkModifierType modifiers = gtk_accelerator_get_default_mod_mask();
        switch (((GdkEventButton *)event)->button) {
        case 1:
            if ((((GdkEventButton *)event)->state & modifiers) ==
                    GDK_CONTROL_MASK &&
                c->hover_uri != NULL) {
                client_new(c->hover_uri, NULL, TRUE, FALSE);
                return TRUE;
            }
            break;
        case 8:
            webkit_web_view_go_back(WEBKIT_WEB_VIEW(c->web_view));
            return TRUE;
        case 9:
            webkit_web_view_go_forward(WEBKIT_WEB_VIEW(c->web_view));
            return TRUE;
        }
    } else if (event->type == GDK_SCROLL) {
        event->scroll.delta_y *= cfg.scroll_lines;
        if (((GdkEventScroll *)event)->state & GDK_CONTROL_MASK) {
            gdk_event_get_scroll_deltas(event, &dx, &dy);
            z = webkit_web_view_get_zoom_level(WEBKIT_WEB_VIEW(c->web_view));
            z += -dy * 0.1;
            z = dx != 0 ? cfg.zoom_level : z;
            webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(c->web_view), z);
            return TRUE;
        }
    }

    return FALSE;
}

void
mainwindow_setup(void) {
    mw.win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(mw.win), 800, 600);
    g_signal_connect(G_OBJECT(mw.win), "destroy", gtk_main_quit, NULL);

    gchar *priv = (cfg.private) ? "-private" : "";
    gchar *title = malloc(strlen(priv) + strlen(__NAME__) + 1);
    sprintf(title, "%s%s", __NAME__, priv);
    gtk_window_set_title(GTK_WINDOW(mw.win), title);
    g_free(title);

    mw.notebook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(mw.notebook), TRUE);
    gtk_container_add(GTK_CONTAINER(mw.win), mw.notebook);
    g_signal_connect(G_OBJECT(mw.notebook), "switch-page",
                     G_CALLBACK(notebook_switch_page), NULL);
}

void
mainwindow_title(gint idx) {
    GtkWidget *child, *widg, *tablabel;
    const gchar *text;

    child = gtk_notebook_get_nth_page(GTK_NOTEBOOK(mw.notebook), idx);
    if (child == NULL)
        return;

    widg = gtk_notebook_get_tab_label(GTK_NOTEBOOK(mw.notebook), child);
    tablabel =
        (GtkWidget *)g_object_get_data(G_OBJECT(widg), "chorizo-tab-label");
    text = gtk_label_get_text(GTK_LABEL(tablabel));
    gtk_window_set_title(GTK_WINDOW(mw.win), text);
}

void
notebook_switch_page(GtkNotebook *nb, GtkWidget *p, guint idx, gpointer data) {
    mainwindow_title(idx);
}

gboolean
quit_if_nothing_active(void) {
    if (clients == 0) {
        if (downloads == 0) {
            gtk_main_quit();
            return TRUE;
        } else {
            downloadmanager_show();
        }
    }

    return FALSE;
}

gboolean
remote_msg(GIOChannel *channel, GIOCondition condition, gpointer data) {
    gchar *uri = NULL;

    g_io_channel_read_line(channel, &uri, NULL, NULL, NULL);
    if (uri) {
        g_strstrip(uri);
        client_new(uri, NULL, TRUE, TRUE);
        g_free(uri);
    }
    return TRUE;
}

void
show_web_view(WebKitWebView *web_view, gpointer data) {
    struct Client *c = (struct Client *)data;
    gint idx;

    (void)web_view;

    gtk_widget_show_all(mw.win);

    if (c->focus_new_tab) {
        idx = gtk_notebook_page_num(GTK_NOTEBOOK(mw.notebook), c->vbox);
        if (idx != -1)
            gtk_notebook_set_current_page(GTK_NOTEBOOK(mw.notebook), idx);

        gtk_widget_grab_focus(c->web_view);
    }
}

void
trust_user_certs(WebKitWebContext *wc) {
    GTlsCertificate *cert;
    gchar *basedir, *absfile;
    const gchar *file;
    GDir *dir = NULL;

    basedir = g_build_filename(g_get_user_data_dir(), __NAME__, "certs", NULL);
    dir = g_dir_open(basedir, 0, NULL);
    g_free(basedir);
    if (dir != NULL) {
        file = g_dir_read_name(dir);
        while (file != NULL) {
            absfile = g_build_filename(g_get_user_data_dir(), __NAME__, "certs",
                                       file, NULL);
            cert = g_tls_certificate_new_from_file(absfile, NULL);
            g_free(absfile);
            if (cert == NULL)
                fprintf(stderr, __NAME__ ": Could not load trusted cert '%s'\n",
                        file);
            else
                webkit_web_context_allow_tls_certificate_for_host(wc, cert,
                                                                  file);
            file = g_dir_read_name(dir);
        }
        g_dir_close(dir);
    }
}

GKeyFile *
get_ini(void) {
    GKeyFileFlags flags = G_KEY_FILE_NONE;
    config = g_key_file_new();

    // Load user config
    gchar *fname = g_build_filename(g_get_user_config_dir(), __NAME__,
                                    "chorizo.ini", NULL);
    if (!g_key_file_load_from_file(config, fname, flags, NULL)) {
        // Load global config
        if (!g_key_file_load_from_file(config, "/etc/chorizo.ini", flags,
                                       NULL)) {
            fprintf(stderr, "Could not load chorizo.ini");
        }
    }
    g_free(fname);
    return config;
}

int
main(int argc, char **argv) {
    int opt, i;

    while ((opt = getopt(argc, argv, "Cpv")) != -1) {
        switch (opt) {
        case 'C':
            cfg.cooperative_instances = FALSE;
            break;
        case 'p':
            cfg.private = true;
            break;
        case 'v':
            printf("%s %s\n", __NAME__, VERSION);
            exit(0);
        default:
            fprintf(stderr, "Usage: " __NAME__ " [OPTION]... [URI]...\n");
            exit(EXIT_FAILURE);
        }
    }

    gtk_init(&argc, &argv);

    // Keep clipboard contents after program closes
    gtk_clipboard_store(gtk_clipboard_get_for_display(gdk_display_get_default(),
                                                      GDK_SELECTION_CLIPBOARD));

    get_config();

    if (cfg.cooperative_instances)
        cooperation_setup();

    if (!cfg.cooperative_instances || cfg.cooperative_alone)
        init_default_web_context();

    downloadmanager_setup();
    mainwindow_setup();

    client_arr = malloc(sizeof(struct Client *));
    if (optind >= argc) {
        client_new(cfg.home_uri, NULL, TRUE, TRUE);
    } else {
        for (i = optind; i < argc; i++)
            client_new(argv[i], NULL, TRUE, TRUE);
    }

    if (!cfg.cooperative_instances || cfg.cooperative_alone)
        gtk_main();

    for (int i = 0; i < clients; i++) {
        free(&(client_arr[i]));
    }

    exit(EXIT_SUCCESS);
}
