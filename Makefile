CFLAGS += -Wall -Wextra -Wno-unused-parameter -O3

PREFIX = /usr/local
bindir = $(DESTDIR)$(PREFIX)/bin
libdir = $(DESTDIR)$(PREFIX)/lib
datadir = $(DESTDIR)$(PREFIX)/share
mandir = $(datadir)/man
docdir = $(datadir)/doc

.PHONY: man clean uninstall install extensions

all: man chorizo extensions darkreader

man:
	for i in man/*.scd; do \
		printf "SCDOC\t%s\n" $$i; \
		scdoc < $$i > $$(echo "$$i" | rev | cut -f 2- -d '.' | rev); \
	done

chorizo:
	$(CC) $(CFLAGS) $(LDFLAGS) \
		-D__NAME__=\"chorizo\" \
		-D__NAME_UPPERCASE__=\"CHORIZO\" \
		-DVERSION=\"v1.0.0\" \
		-o chorizo src/*.c \
		`pkg-config --cflags --libs gtk+-3.0 glib-2.0 webkit2gtk-4.0`

extensions:
	for i in extensions/*.c; do \
		outp=$$(echo "$$i" | sed 's/\$\.c/.so/g'); \
		$(CC) $(CFLAGS) $(LDFLAGS) \
			-D__NAME__=\"chorizo\" \
			-D__NAME_UPPERCASE__=\"CHORIZO\" \
			-shared -o $$outp -fPIC $$i \
			`pkg-config --cflags --libs glib-2.0 webkit2gtk-4.0`; \
	done

install: all
	mkdir -p $(bindir) \
		$(mandir)/man1 \
		$(mandir)/man5 \
		$(libdir)/chorizo/web-extensions \
		$(datadir)/chorizo/user-scripts \
		$(datadir)/applications \
		$(docdir)/chorizo

	cp chorizo $(bindir)/
	cp man/*.1 $(mandir)/man1/
	cp man/*.5 $(mandir)/man5/
	cp chorizo.ini $(docdir)/chorizo/
	cp chorizo.desktop $(datadir)/applications/
	cp -r extensions/*.so $(libdir)/chorizo/web-extensions/
	cp -r user-scripts/* $(datadir)/chorizo/user-scripts/

uninstall:
	rm -rf $(bindir)/chorizo \
		$(libdir)/chorizo \
		$(mandir)/man1/chorizo* \
		$(mandir)/man5/chorizo* \
		$(datadir)/chorizo \
		$(datadir)/applications/chorizo.desktop \
		$(docdir)/chorizo

reinstall: uninstall install

darkreader:
	curl -L "https://cdn.jsdelivr.net/npm/darkreader/darkreader.min.js" \
		-o user-scripts/darkreader.js
	echo >> user-scripts/darkreader.js
	echo 'DarkReader.enable({brightness:100,contrast:100,sepia:0});' \
		>> user-scripts/darkreader.js

clean:
	rm -fv chorizo \
		extensions/*.so \
		man/*.1 \
		man/*.5 \
		user-scripts/darkreader.js
